# Welcome to my portfolio ©️

I'm Developer and Entrepreneur technology with focus on Bitcoin, Lightning Network, Sidechain, Drivechain, Atomic Swap ⚡

# Background 

I started in 2018 when I met Ethereum there  I learned about Solidity and create some tokens. But after discover Atomic Swap, Bitcoin and Lightning Network, mainly with Taproot I seen new market raising.

# Portifolio

*Marketplace P2P: https://github.com/Layer2Labs/P2PBTC

*Marketplace NFT: https://github.com/Layer2Labs/ArtByte

*Stablecoin: https://github.com/Layer2Labs/Jet

*Security Tokens: https://github.com/Layer2Labs/SecurityTokens

*Exchange Decentralized: https://github.com/PorticoExchange

*Lightning Name Service: https://github.com/Layer2Labs/LightningNameService

*Index token: https://github.com/Layer2Labs/Index
